use std::io::Write;
use vvcore::*;
use std::env;

fn main() {
    let dir = std::ffi::CString::new("open_jtalk_dic_utf_8-1.11").unwrap();
    let vvc = VoicevoxCore::new_from_options(AccelerationMode::Auto, 0, true, dir.as_c_str()).unwrap();

    let args: Vec<String> = env::args().collect();

    let speaker: u32 = env::args().nth(1).unwrap().parse::<u32>().unwrap();       // 1;
    let text: &str = &args[2];                          // "こんにちは";
    let wav = vvc.tts_simple(text, speaker).unwrap();

    let mut file = std::fs::File::create(&args[3]).unwrap();    // "audio.wav"
    file.write_all(&wav.as_slice()).unwrap();
}


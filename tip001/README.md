# 手順めも

### 1.
```
gitpod /workspace/voicevox/tip001 (main) $ binary=download-linux-x64
gitpod /workspace/voicevox/tip001 (main) $ curl -sSfL https://github.com/VOICEVOX/voicevox_core/releases/latest/download/${binary} -o download
gitpod /workspace/voicevox/tip001 (main) $ ll
total 4960
drwxr-xr-x 2 gitpod gitpod      81 Sep 18 05:40 ./
drwxr-xr-x 4 gitpod gitpod      49 Sep 18 05:25 ../
-rw-r--r-- 1 gitpod gitpod 5070696 Sep 18 05:40 download
-rw-r--r-- 1 gitpod gitpod       0 Sep 18 05:25 .gitkeep
-rwxr-xr-x 1 gitpod gitpod    1519 Aug 15 15:37 linuxInstallCpu.sh*
-rw-r--r-- 1 gitpod gitpod    1360 Sep 18 05:38 README.md
gitpod /workspace/voicevox/tip001 (main) $ chmod +x download
gitpod /workspace/voicevox/tip001 (main) $ ./download
 INFO 対象OS: linux
 INFO 対象CPUアーキテクチャ: x64
 INFO ダウンロードデバイスタイプ: cpu
 INFO ダウンロードvoicevox_coreバージョン: 0.14.4
voicevox_core-linux-x64-cpu-0.14.4.zip                    Done!
open_jtalk_dic_utf_8-1.11.tar.gz                          Done!                                                                                                                                                  INFO 全ての必要なファイルダウンロードが完了しました
gitpod /workspace/voicevox/tip001 (main) $ 
itpod /workspace/voicevox/tip001 (main) $ ll
total 4960
drwxr-xr-x 3 gitpod gitpod     102 Sep 18 05:41 ./
drwxr-xr-x 4 gitpod gitpod      49 Sep 18 05:25 ../
-rwxr-xr-x 1 gitpod gitpod 5070696 Sep 18 05:40 download*
-rw-r--r-- 1 gitpod gitpod       0 Sep 18 05:25 .gitkeep
-rwxr-xr-x 1 gitpod gitpod    1519 Aug 15 15:37 linuxInstallCpu.sh*
-rw-r--r-- 1 gitpod gitpod    2668 Sep 18 05:42 README.md
drwxr-xr-x 4 gitpod gitpod     167 Sep 18 05:41 voicevox_core/
gitpod /workspace/voicevox/tip001 (main) $ 
gitpod /workspace/voicevox/tip001 (main) $ ls -l voicevox_core
total 17452
-rw-r--r-- 1 gitpod gitpod 15201616 Sep 18 05:41 libonnxruntime.so.1.13.1
-rw-r--r-- 1 gitpod gitpod  2623160 Sep 18 05:41 libvoicevox_core.so
drwxr-xr-x 2 gitpod gitpod     4096 Sep 18 05:41 model
drwxr-xr-x 2 gitpod gitpod      161 Sep 18 05:41 open_jtalk_dic_utf_8-1.11
-rw-r--r-- 1 gitpod gitpod     9816 Sep 18 05:41 README.txt
-rw-r--r-- 1 gitpod gitpod        7 Sep 18 05:41 VERSION
-rw-r--r-- 1 gitpod gitpod    16954 Sep 18 05:41 voicevox_core.h
gitpod /workspace/voicevox/tip001 (main) $ 
```

### 2
ここからが動かない..
```
pip install https://github.com/VOICEVOX/voicevox_core/releases/download/0.14.4/voicevox_core-0.14.4+cpu-cp38-abi3-linux_x86_64.whl
binary=download-linux-x64
curl -sSfL https://github.com/VOICEVOX/voicevox_core/releases/latest/download/${binary} -o download
chmod +x download
./download -o ./example/python
# export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/workspace/voicevox/tip001/voicevox_core/libonnxruntime.so.1.13.1
python ./run.py -h
``````

#### python ./run.py -h
```
gitpod /workspace/voicevox/tip001 (main) $ ll
total 4964
drwxr-xr-x 4 gitpod gitpod     131 Sep 18 06:01 ./
drwxr-xr-x 4 gitpod gitpod      49 Sep 18 05:25 ../
-rwxr-xr-x 1 gitpod gitpod 5070696 Sep 18 05:54 download*
drwxr-xr-x 3 gitpod gitpod      20 Sep 18 05:55 example/
-rw-r--r-- 1 gitpod gitpod       0 Sep 18 05:25 .gitkeep
-rwxr-xr-x 1 gitpod gitpod    1519 Aug 15 15:37 linuxInstallCpu.sh*
-rw-r--r-- 1 gitpod gitpod    2906 Sep 18 06:15 README.md
-rw-r--r-- 1 gitpod gitpod    2928 Sep 18 06:01 run.py
drwxr-xr-x 4 gitpod gitpod     167 Sep 18 05:41 voicevox_core/
gitpod /workspace/voicevox/tip001 (main) $ python run.py -h
Traceback (most recent call last):
  File "/workspace/voicevox/tip001/run.py", line 9, in <module>
    import voicevox_core
  File "/workspace/.pyenv_mirror/user/current/lib/python3.11/site-packages/voicevox_core/__init__.py", line 11, in <module>
    from ._rust import METAS, SUPPORTED_DEVICES, VoicevoxCore  # noqa: F401
    ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
ImportError: libonnxruntime.so.1.13.1: cannot open shared object file: No such file or directory
gitpod /workspace/voicevox/tip001 (main) $ 
```
